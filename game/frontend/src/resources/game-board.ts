import {bindable, autoinject} from 'aurelia-framework';

@autoinject
export class GameBoard {

  @bindable current_player: string; // can be 'X' or 'O'
  @bindable board: string;   // board state
  @bindable winner: string;  // if there is a winner 'X' or 'O' will be here
  @bindable restart_url: string;  // url to restart the game
  @bindable test: string;  // url to restart the game


  cells: Array<string> = [];
  form: HTMLFormElement;
  current_player_input: HTMLInputElement;
  board_input: HTMLInputElement;
  component: Element;

  bind() {
    //convert string to array of cells
    this.cells = this.board.split('');
  }

  attached() {
    // attach the form elements manually
    this.form = this.component.getElementsByTagName('form')[0];
    this.current_player_input = document.getElementById('id_current_player') as HTMLInputElement;
    this.board_input = document.getElementById('id_board') as HTMLInputElement;
    console.log(this.test)
  }

  cellClick(cellNumber: number) {
    // put the X or O in the cells array
    this.cells[cellNumber] = this.current_player;
    // convert the cells array to a string and put it in the django form input element
    this.board_input.value = this.cells.join('');
    // put the current player into the django form input element
    this.current_player_input.value = this.current_player;
    // submit the form
    this.form.submit();
  }

  playAgain() {
    window.location.href = this.restart_url;
  }
}
