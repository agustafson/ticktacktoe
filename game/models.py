from django.db import models
from django.urls import reverse


class Game(models.Model):

    current_player = models.CharField(
        max_length=9,
        choices=(('X', 'X'), ('O', 'O')),
        default='X',
    )

    board = models.CharField(
        max_length=9,
        default='---------',
    )

    def get_absolute_url(self):
        return reverse('game-update', kwargs={'pk': self.pk})

