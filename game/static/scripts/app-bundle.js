define('resources',['resources/index'],function(m){return m;});;
define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true
    };
});
;
define('main',["require", "exports", "./environment"], function (require, exports, environment_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .feature('resources');
        aurelia.use.developmentLogging(environment_1.default.debug ? 'debug' : 'warn');
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(function () { return aurelia.enhance(); });
    }
    exports.configure = configure;
});
;
define('resources/app',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var App = (function () {
        function App() {
            this.message = 'Hello World!';
        }
        return App;
    }());
    exports.App = App;
});
;
define('resources/app.html!text',[],function(){return "<template>\n  <h1>${message}</h1>\n</template>\n";});;
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('resources/game-board',["require", "exports", "aurelia-framework"], function (require, exports, aurelia_framework_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GameBoard = (function () {
        function GameBoard() {
            this.cells = [];
        }
        GameBoard.prototype.bind = function () {
            this.cells = this.board.split('');
        };
        GameBoard.prototype.attached = function () {
            this.form = this.component.getElementsByTagName('form')[0];
            this.current_player_input = document.getElementById('id_current_player');
            this.board_input = document.getElementById('id_board');
            console.log(this.test);
        };
        GameBoard.prototype.cellClick = function (cellNumber) {
            this.cells[cellNumber] = this.current_player;
            this.board_input.value = this.cells.join('');
            this.current_player_input.value = this.current_player;
            this.form.submit();
        };
        GameBoard.prototype.playAgain = function () {
            window.location.href = this.restart_url;
        };
        __decorate([
            aurelia_framework_1.bindable,
            __metadata("design:type", String)
        ], GameBoard.prototype, "current_player", void 0);
        __decorate([
            aurelia_framework_1.bindable,
            __metadata("design:type", String)
        ], GameBoard.prototype, "board", void 0);
        __decorate([
            aurelia_framework_1.bindable,
            __metadata("design:type", String)
        ], GameBoard.prototype, "winner", void 0);
        __decorate([
            aurelia_framework_1.bindable,
            __metadata("design:type", String)
        ], GameBoard.prototype, "restart_url", void 0);
        __decorate([
            aurelia_framework_1.bindable,
            __metadata("design:type", String)
        ], GameBoard.prototype, "test", void 0);
        GameBoard = __decorate([
            aurelia_framework_1.autoinject
        ], GameBoard);
        return GameBoard;
    }());
    exports.GameBoard = GameBoard;
});
;
define('resources/game-board.css!text',[],function(){return "game-board .board{width:90vw;height:90vw;margin-left:auto;margin-right:auto;background-color:#5b80b2;flex-direction:column}game-board .board,game-board .board .row{display:flex;justify-content:space-between}game-board .board .row{width:100%;flex-direction:row;flex-wrap:nowrap}game-board .board .row .cell{height:28vw;width:28vw;background-color:#fff;font-family:sans-serif;font-size:15vw;display:flex;flex-direction:row;justify-content:center;align-items:center}game-board .board .row .cell:hover{background-color:pink}game-board .round{height:4vw;text-align:center;margin-bottom:1em}game-board .round,game-board .winner{width:90vw;margin-left:auto;margin-right:auto}game-board .winner{height:90vw;background-color:#ff00a1;color:#fff;display:flex;justify-content:center;align-content:center;flex-direction:column;align-items:center;font-size:5vw}";});;
define('resources/game-board.html!text',[],function(){return "<template ref=\"component\">\n  <require from=\"./game-board.css\"></require>\n\n  <div class=\"round\">\n    <h1 if.bind=\"!winner\">${current_player}'s Turn</h1>\n  </div>\n\n  <div class=\"board\" if.bind=\"!winner\">\n    <div class=\"row\">\n      <div class=\"cell\" click.delegate=\"cellClick(0)\">${cells[0]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(1)\">${cells[1]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(2)\">${cells[2]}</div>\n    </div>\n    <div class=\"row\">\n      <div class=\"cell\" click.delegate=\"cellClick(3)\">${cells[3]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(4)\">${cells[4]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(5)\">${cells[5]}</div>\n    </div>\n    <div class=\"row\">\n      <div class=\"cell\" click.delegate=\"cellClick(6)\">${cells[6]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(7)\">${cells[7]}</div>\n      <div class=\"cell\" click.delegate=\"cellClick(8)\">${cells[8]}</div>\n    </div>\n  </div>\n\n  <div if.bind=\"winner\" class=\"winner\">\n    <h1>${winner} is the winner!</h1>\n  </div>\n  <div if.bind=\"winner\">\n    <button click.delegate=\"playAgain()\">Play Again?</button>\n  </div>\n\n  <slot></slot>\n</template>\n";});;
define('resources/index',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(config) {
        config.globalResources([
            './game-board',
            './app',
        ]);
    }
    exports.configure = configure;
});

//# sourceMappingURL=app-bundle.js.map