from django.views.generic.edit import CreateView, UpdateView
from .models import Game
from django.urls import reverse


class StartGame(CreateView):
    model = Game
    fields = ['current_player', 'board',]
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_player'] = context['form'].fields['current_player'].initial
        context['board'] = context['form'].fields['board'].initial
        context['winner'] = ''
        return context


class UpdateGame(UpdateView):
    model = Game
    fields = ['current_player', 'board', ]
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # toggle the current player
        context['current_player'] = 'X' if self.object.current_player == 'O' else 'O'
        context['board'] = self.object.board
        context['restart_url'] = reverse('game-start')

        # simple board validation to check if three in row
        board = list(self.object.board)
        context['winner'] = ''
        if (board[0] != '-' and len(set([board[0], board[3], board[6]])) == 1
                or board[0] != '-' and len(set([board[0], board[1], board[2]])) <= 1
                or board[0] != '-' and len(set([board[0], board[4], board[8]])) <= 1):
            context['winner'] = board[0]

        if board[1] != '-' and len(set([board[1], board[4], board[7]])) <= 1:
            context['winner'] = board[1]

        if (board[2] != '-' and len(set([board[2], board[4], board[6]])) <= 1
                or board[2] != '-' and len(set([board[2], board[5], board[8]])) <= 1):
            context['winner'] = board[2]

        if board[3] != '-' and len(set([board[3], board[4], board[5]])) <= 1:
            context['winner'] = board[3]

        if board[6] != '-' and len(set([board[6], board[7], board[8]])) <= 1:
            context['winner'] = board[6]

        return context
