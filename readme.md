# ticktacktoe

We're going to create a tic-tack-toe game with Django and Aurelia to demonstrate how these two wonderful frameworks work together nicely. You might be reading this coming from a Django background or coming from a Aurelia or JS background, so I'll try to cover some of the basics of both.

To being with we need to make sure we have the basic requirements installed. I'll just asume you have Python 3.6 or higher installed already, as well as pip. That covers the django side of things. For Aurelia you need to make sure you have an up to date version of Node installed. I'll be using NPM to install dependencies though I prefer Yarn for real projects. 

Open a terminal and navigate to your prefered project directory.

## Initial Setup

```shell
cd into/some/directory/
```

Install the python package virtualenv. This will help keep your system uncluttered by anything that we install herafter. 
```shell
pip install virtualenv
```
Install the Aurelia CLI globally. 
```shell
npm install aurelia-cli -g
```
Create and cd into the project directory.
```shell
mkdir tictacktoe
cd ticktacktoe
```
Create and activate your local python environment. Then install the Django package into the Python environment.
```shell
virtualenv -p python3 venv
source venv/bin/activate
pip install Django
```
Create the main Django project
```shell
django-admin startproject web .
```

Create the Game app module. This will be the Django App that contains all the frontend bits, 
```shell
django-admin startapp game
```

Within the Game app we'll create the frontend directory that will contain all the aurelia stuff. We'll also create the template and static directories for django template files and js and css files that are served to the browser. 
```shell
cd game
mkdir static
mkdir templates
touch templates/home.html
au new
    - enter the following aurelia options
    - name : frontend
    - setup: Custom App
    - bundler: CLIs built-in
    - AMD loader: SystemJS
    - target platofrm: Web
    - transpiler: Typescript
    - minification: None
    - css preprocessor: Sass
    - PostCSS processing: Typical
    - test runner: None ( yeah, i know, but its just an example!)
    - integration testing: None again
    - code editor: none
    - scaffolded: Minimum
    - dockerfile: No
    - npm dependencies: Yes, use NPM

    - ok wait
```
Now we have all the frameworks installed and setup, we have to hook everything up to get started.


## Django Setup

From the game directory, open the models.py files in a text editor or IDE and enter the following:

```python
# ticktacktoe/web/game/models.py
from django.db import models


class Game(models.Model):

    current_player = models.CharField(
        max_length=9,
        choices=(('X', 'X'), ('O', 'O')),
        default='X',
    )

    board = models.CharField(
        max_length=9,
        default='.........',
    )
```
We're defining a simple game state model with two fields. The first field current_player can have the values 
X or O. This is be toggled everytime a round is saved. board stores the state of the tick tack toe field, 9 
slots, can be '-' (empty), 'X' or 'O'.

Once the models are defined, lets hook up the Game module to the main Django Web app. Go into the
web/settings.py file and add 'game' to the list of installed apps.

```python
# ticktacktoe/web/settings.py
...

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'game',  # <-- reference the game app here
]
...
```

Lets create the database with the tables for the game state. From the root ticktacktoe folder run the 
following command:

```shell script
python manage.py makemigrations
python manage.py migrate
```

You should see a list of migrations that are being run against the newly created sqlite database. The
database file should be visible in the root folder. db.sqlite3.

From the game directory, open the views.py file and enter the following:
```python
# ticktacktoe/web/game/views.py
from django.views.generic.edit import CreateView, UpdateView
from .models import Game
from django.urls import reverse


class StartGame(CreateView):
    model = Game
    fields = ['current_player', 'board',]
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_player'] = context['form'].fields['current_player'].initial
        context['board'] = context['form'].fields['board'].initial
        context['winner'] = ''
        return context


class UpdateGame(UpdateView):
    model = Game
    fields = ['current_player', 'board', ]
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # toggle the current player
        context['current_player'] = 'X' if self.object.current_player == 'O' else 'O'
        context['board'] = self.object.board
        context['restart_url'] = reverse('game-start')

        # simple board validation to check if three in row
        board = list(self.object.board)
        context['winner'] = ''
        if (board[0] != '-' and len(set([board[0], board[3], board[6]])) == 1
                or board[0] != '-' and len(set([board[0], board[1], board[2]])) <= 1
                or board[0] != '-' and len(set([board[0], board[4], board[8]])) <= 1):
            context['winner'] = board[0]

        if board[1] != '-' and len(set([board[1], board[4], board[7]])) <= 1:
            context['winner'] = board[1]

        if (board[2] != '-' and len(set([board[2], board[4], board[6]])) <= 1
                or board[2] != '-' and len(set([board[2], board[5], board[8]])) <= 1):
            context['winner'] = board[2]

        if board[3] != '-' and len(set([board[3], board[4], board[5]])) <= 1:
            context['winner'] = board[3]

        if board[6] != '-' and len(set([board[6], board[7], board[8]])) <= 1:
            context['winner'] = board[6]

        return context
```

These are Django Generic Views for rendering model forms that will create or update a data model. 
The View attributes set which model to use, which fields will be available in the form, and the name of the
html template that will display the form. 

We're adding some data to the view context to notify the frontend which is the current user and whether anyone
has won the game, and what the url is to restart the game. The board state is an array of nine character. To validata
if three are in a row, 5 key positions are checked to be not empty, and if so does is have the same value as any other
3 that might be in a row.

Let create that home.html template. The file was created in the initial setup above. Open it in a
text editor and add this text:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tick Tack Toe</title>
</head>
<body>

    <form method="post">{% csrf_token %}
        {{ form.as_p }}
        <input type="submit" value="Update">
    </form>

</body>
</html>
```

We need to link these views to a url pattern. 

Open the url.py file located in the web folder and add the routed to the StartGame and UpdateGame views.

```python
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from game.views import StartGame, UpdateGame

urlpatterns = [
    path('', StartGame.as_view(), name='game-start'),
    path('<int:pk>/', UpdateGame.as_view(), name='game-update'),
    url(r'^admin/', admin.site.urls),
]
```

Lets start the django server and see what have so far.
From the root ticktacktoe folder run the following command:

```shell script
python manage.py runserver
```

Open your browser to url: 127.0.0.1:8000 and you should see a page with 2 form elements and a button
labeled Update. Django has automatically created this form based on the definition of the data
model, and the attibutes defined in the CreateView class.

If you try to submit the form you'll get an error. Django is telling you it does not know what page url
to load after a successful update. Lets add a 'get_absolute_url' to the Game Model definition.

Change your game/models.py file to the following:

```python
from django.db import models
from django.urls import reverse


class Game(models.Model):

    current_player = models.CharField(
        max_length=9,
        choices=(('X', 'X'), ('O', 'O')),
        default='X',
    )

    board = models.CharField(
        max_length=9,
        default='000000000',
    )

    def get_absolute_url(self):
        return reverse('game-update', kwargs={'pk': self.pk})
```

The reverse method is given a reference to the update url path defined in the urls.py file and
generates the corresponding url string. Django 
references routes by name. Routes can change over the course of project development. It's great to 
be able to reference the routes by name, otherwise you would have to go and modify all the hardcoded
href attributes by hand. Aurelia also names it's routes, that's great. Angular has caused me much pain,
and I have cried outloud "How can they not know??". 

Now that we can update the game data nicely, lets do some Aurelia stuff.


## Aurelia Setup

By default aurelia will compile it's js files to a scripts folder in the root folder of the aurelia 
project ( for us that's ticktacktoe/game/frontend ). What we would like to do is keep all the 
aurelia uncompiled stuff in the frontend folder. The compiled stuff should be moved to the "static"
directory, one level up in the hierarchy.

Open the aurelia.json file that is located in ticktacktoe/game/frontend/aurelia_project/ directory.

find the cssProcessor part and add an output path:
```json
  "cssProcessor": {
    "source": [
      "src/**/*.scss"
    ],
    "output": [
      "../static/styles/"
    ]
  },
```

find the platform and build sections and baseURL and output paths:

```json
  "platform": {
    "port": 9000,
    "host": "localhost",
    "open": false,
    "index": "index.html",
    "baseDir": ".",
    "output": "../static/scripts",  <-- change
    "baseUrl": "../static/scripts"  <-- change
  },
  "build": {
    "targets": [
      {
        "port": 9000,
        "index": "index.html",
        "baseDir": ".",
        "output": "../static/scripts",  <-- change
        "baseUrl": "../static/scripts"  <-- change
      }
    ],
    "options": {
      "minify": "stage & prod",
      "sourcemaps": "dev & stage",
      "rev": false,  <-- change
      "cache": "dev & stage"
    },
```

Run the Aurelia CLI builder in the frontend folder to test what happens. 

```shell script
npm run build
```

You should see the app-bundle.js and vendor-bundle.js files in the game/static/scripts directory.
Lets reference these files in the home.html file and add the aurelia tags.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tick Tack Toe</title>
</head>
<body aurelia-app="main">  <!-- << add aurelia-app attribute -->

    <form method="post">{% csrf_token %}
        {{ form.as_p }}
        <input type="submit" value="Update">
    </form>

    <!-- add scripts -->
    <script src="/static/scripts/vendor-bundle.js"></script> <!-- << path with /static/ -->
    <script>SystemJS.import("aurelia-bootstrapper").catch(function(err) { console.log(err); })</script>

</body>
</html>
```

Reload the page in your browser and you should now see that standard Aurelia "Hello World!" message.
The django content has been completely replaced by the aurelia content. That's not quite what we
wanted though.

Let's make a couple more changes. Just after the <body> tag, add an <app> tag to your home.html
file:

```html
..
..
<body aurelia-app="main">  

    <app></app> <!-- add app tag -->

    <form method="post">{% csrf_token %}
    ..
    ..
..
```

Make the following change to the frontend/src/main.ts file:

```typescript
import {Aurelia} from 'aurelia-framework'
import environment from './environment';

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources');

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.enhance());  // <-- change to enhance
}

```


Move the app.html and app.ts files from the frontend/src directory to the frontend/src/resources dir. The 
default project settings in Aurelia will automatically load and attach the app component. By changing the
aurelia behavior to enhance, the default behavior 
is changed, we need to explicitly reference and load the app component now.  It no longer has any special
"app" meaning, could be "foo", and is a normal Aurelia component. We need to reference it in resources/index.ts
file, so that it is available globally.

In the frontend/src/resources folder, locate the index.ts file and reference the app component.

```typescript
import {FrameworkConfiguration} from 'aurelia-framework';

export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    './app',  
  ]);
}
```
run the build command from the frontend directory:

```shell script
npm run build
```
Reload your browser window and you should now see the "Hello World!" message and the django form. You can
change values in the input elements, click "Update" and see that the values are save and persisted upon
reload.

What's the app component for? Well, right now it's just to test that aurelia and djagno have play together
nicely on the same page. 

Ok, we're finished with the initial setup. Lets dig into Aurelia and see how it can really enhance the
page.


## Enhance the Frontend

User the Aurelia CLI to create a new game component. This component will encapsulate the frontend logic and 
UI structure. We'll also add a game-board.scss file to manage the layout of the game board component.

From the frontend directory run the following in your terminal:

```shell script
au generate component game-board
 - as default folder type: ./resources
touch ./src/resources/game-board.scss
```
reference the game-board component in the fronten/src/resources/index.ts file:

```typescript
import {FrameworkConfiguration} from 'aurelia-framework';

export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    './game-board',
    './app',  
  ]);
}
```

We are going to use the game-board component to render the tick tack toe board, wrap the django form element,
hide it, and manage it's input value. 

This is what your game board html, ts, and scss files need to look like:

```html
<template ref="component">
  <require from="./game-board.css"></require>

  <div class="round">
    <h1 if.bind="!winner">${current_player}'s Turn</h1>
  </div>

  <div class="board" if.bind="!winner">
    <div class="row">
      <div class="cell" click.delegate="cellClick(0)">${cells[0]}</div>
      <div class="cell" click.delegate="cellClick(1)">${cells[1]}</div>
      <div class="cell" click.delegate="cellClick(2)">${cells[2]}</div>
    </div>
    <div class="row">
      <div class="cell" click.delegate="cellClick(3)">${cells[3]}</div>
      <div class="cell" click.delegate="cellClick(4)">${cells[4]}</div>
      <div class="cell" click.delegate="cellClick(5)">${cells[5]}</div>
    </div>
    <div class="row">
      <div class="cell" click.delegate="cellClick(6)">${cells[6]}</div>
      <div class="cell" click.delegate="cellClick(7)">${cells[7]}</div>
      <div class="cell" click.delegate="cellClick(8)">${cells[8]}</div>
    </div>
  </div>

  <div if.bind="winner" class="winner">
    <h1>${winner} is the winner!</h1>
  </div>
  <div if.bind="winner">
    <button click.delegate="playAgain()">Play Again?</button>
  </div>

  <slot></slot>
</template>
```

The .board element will display the tick tack toe state. Clicking a cell will trigger the cellClick() method in the
game-board view controller, passing the index of the clicked cell. You could generate the board with fancy nested
repeat.for attributes, but I want to keep things easy to look at for non Aurealia users. 

The various if.bind attributes just remove or reveal elements based on whether the is a winner. The winner is evaluated
by the Django backend and passed as a context variable to the template. We'll see how this is connected a little later.

Here is scss for the template:

```scss
game-board {
  .board {
    width: 90vw;
    height: 90vw;
    margin-left: auto;
    margin-right: auto;
    background-color: #5b80b2;
    display: flex;
    justify-content: space-between;
    flex-direction: column;

    .row {
      width: 100%;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      justify-content: space-between;
      .cell {
        height: 28vw;
        width: 28vw;
        background-color: white;
        font-family: sans-serif;
        font-size: 15vw;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        &:hover {
          background-color: pink;
        }
      }
    }
  }
  .round {
    width: 90vw;
    height: 4vw;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    margin-bottom: 1em;
  }
  .winner {
    width: 90vw;
    height: 90vw;
    margin-left: auto;
    margin-right: auto;
    background-color: #ff00a1;
    color: white;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
    align-items: center;
    font-size: 5vw;
  }
}
```
 Here's the Aurelia view controller for the game board:
 
```typescript
import {bindable, autoinject} from 'aurelia-framework';

@autoinject
export class GameBoard {

  @bindable current_player: string; // can be 'X' or 'O'
  @bindable board: string;   // board state
  @bindable winner: string;  // if there is a winner 'X' or 'O' will be here
  @bindable restart_url: string;  // url to restart the gam

  cells: Array<string> = [];
  form: HTMLFormElement;
  current_player_input: HTMLInputElement;
  board_input: HTMLInputElement;
  component: Element;

  bind() {
    //convert string to array of cells
    this.cells = this.board.split('');
  }

  attached() {
    // attach the form elements manually
    this.form = this.component.getElementsByTagName('form')[0];
    this.current_player_input = document.getElementById('id_current_player') as HTMLInputElement;
    this.board_input = document.getElementById('id_board') as HTMLInputElement;
  }

  cellClick(cellNumber: number) {
    // put the X or O in the cells array
    this.cells[cellNumber] = this.current_player;
    // convert the cells array to a string and put it in the django form input element
    this.board_input.value = this.cells.join('');
    // put the current player into the django form input element
    this.current_player_input.value = this.current_player;
    // submit the form
    this.form.submit();
  }

  playAgain() {
    window.location.href = this.restart_url;
  }
}

```
 There are 4 bindable attributes, current_player, board, winner and restart_url. We've seen these values being 
 prepared in the get_content methos of the Django Views. Where the board component is in the bind() lifecycle 
 phase, we get the 'board' attribtue value and split the string into an array and store that in the 'cells' attribute.
 
 After the component has been attached to the DOM the attached() method goes and gathers references to the Django
 Form Input Elements. 
 
 When a cell is clicked, data is transformed back to strings and slipped back into the Form Elements before we submit
 the form back to the Django application.
 
 The last step is to modifiy the Django home.html template so that the context values are passed to the aurlia component,
 and the form is wrapped by the Aurelia component. We'll also set a display none on the form, otherwise it might breifly
 be visible before the Aurelia bits have been loaded. 

Here is the updated home.html template:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tick Tack Toe</title>
</head>
<body aurelia-app="main">

    <game-board current_player="{{ current_player }}"
                board="{{ board }}"
                winner="{{ winner }}"
                restart_url="{{ restart_url }}">

        <form method="post" style="display: none">
            {% csrf_token %}
            {{ form.as_p }}
            <input type="submit" value="Update">
        </form>

    </game-board>

    <script src="/static/scripts/vendor-bundle.js"></script>
    <script>SystemJS.import("aurelia-bootstrapper").catch(function(err) { console.log(err); })</script>

</body>
</html>
```

Rebuild the Aurelia project from the frontend/ directory

```shell script
npm run build
```

Your Django server might still be running, if not restart it by running the following from the root ticktacktoe 
directory:

```shell script
python manage.py runserver
```

Play tick tack toe in your browser

## Summary

There's a lot of room for improvement, some validation is missing, the binding between the Django Form and the Aurelia
Component is clumsy, contrived, and could be easily done with jquery or pure javascript, there's no live reloading, and
the project layout does not really represent how a production ready django project should be setup.  
What I really wanted to show though is how to get Aurelia working nicely on a Django page. The key parts are really 
just the changes made in aurelia.json and main.ts. That's what allows enhancing the page.

I've used similar setups, with Nunjucks.js instead of Django. I was working with a HTML/CSS Frontend Designer on a 
project. He could work on all the HTML/SCSS parts, without ever needing to go into the aurelia folder, The HTML/CSS Designer 
was repsonsible for the global style, look and feel. I could develop all the interactive components ( special dropdowns, 
accordions, animated widgets and teaser, etc ) with a minimum of css just to set the structure.  
This experience helped me to get a sense of where to draw the line between the Django and Aurelia parts. 

In a real project I would use django-compressor to pull in and compile the site wide SCSS/SASS, defining the theme
of the page (colors, font sizes, paddings, etc) . The scss in Aurelia would just define the structure of the individual 
components and element. I would also be using Django Rest Framework to build an api to pass data to and from the server. 
Validation works nicely through DRF, you get the error message in whatever lanugages are setup. You can query the API 
with an OPTIONS method, and parse the results to get you information for labels and input types, allowing you to 
dynamically generate forms in Aurelia. This means you're dont have to recreate all your model definitions in the frontend 
and keep them in sync every time something changes. Let Django or even Django-CMS provide the content and menu structure 
on the page. I just enjoy it so much. 

## First Bonus Round - Live Reloading

By default Aurelia has a file watcher that will reload a page when changes are detected. By using Django
to load the page, we're bypassing this nice feature. We can run aurelia's built-in browser-sync server,
with file watching enabled, and still serve the content through django.

from the game/frontend/ directory run the following command in the console:

```shell script
npm install http-proxy-middleware --save-dev
```

Now with http-proxy-middleware downloaded, lets modifiy aurelia's default run task.

Open aurelia run.ts file locates in the game/frontend/aurelia_project/tasks directory
```typescript
import * as gulp from 'gulp';
import * as browserSync from 'browser-sync';
import * as historyApiFallback from 'connect-history-api-fallback/lib';
import * as project from '../aurelia.json';
import {CLIOptions} from 'aurelia-cli';
import build from './build';
import watch from './watch';

const proxy = require('http-proxy-middleware');  <-- add this
const django = proxy('/', {target: 'http://127.0.0.1:8000'}); <-- add this


const bs = browserSync.create();

let serve = gulp.series(
  build,
  done => {
    bs.init({
      online: false,
      open: CLIOptions.hasFlag('open') || project.platform.open,
      port: CLIOptions.getFlagValue('port') || project.platform.port,
      host: CLIOptions.getFlagValue('host') || project.platform.host || "localhost",
      logLevel: 'silent',
      server: {
        baseDir: [project.platform.baseDir],
        middleware: [historyApiFallback(), function(req, res, next) {
          res.setHeader('Access-Control-Allow-Origin', '*');
          next();
        }, django] <-- add this
      }
    }, function (err, bs) {
      if (err) return done(err);
      let urls = bs.options.get('urls').toJS();
    ...
    ..
    .
```

Make sure that the port matches the port that your Django server ist listening on. Start your 
Django server in one terminal. Then in another terminal, start the aurelia live reload server:

```shell script
npm run start
```

Browser Sync will indicated in the terminal which ip and port it is listening on. Should look
something like this:

````shell script
Application Available At: http://localhost:9001
````

Open a browser tab to that page, you should see your django application. Make some changes in 
your game-board.scss file and watch the browser reload with those changes.


## Second Bonus Round - makeing the aurelia bundle smaller

Coming soon.


## Third Bonus Round - using replaceable and custom django forms to integrate form data with aurelia more elegantly.

Coming soon.
