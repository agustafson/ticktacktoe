from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from game.views import StartGame, UpdateGame

urlpatterns = [
    path('', StartGame.as_view(), name='game-start'),
    path('<int:pk>/', UpdateGame.as_view(), name='game-update'),
    url(r'^admin/', admin.site.urls),
]
